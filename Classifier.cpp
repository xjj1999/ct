#include "Classifier.h"
#include <opencv2/opencv.hpp>
#undef slots
#include <torch/torch.h>
#define slots Q_SLOTS
#include <torch/script.h>
#include <exception>

Classifier::Classifier(const std::string& model_file, const std::string& label_file, torch::DeviceType dt) {

    // set device type and device, cpu inference or gpu inference
    device_type = dt;
    torch::Device device(device_type);

    /* Load Module from model file path */
	torch::jit::script::Module module = torch::jit::load(model_file, device);
    module.eval();

    /* Load class name from class name txt */
    labels_ = loadClassNames(label_file);

}

std::vector<std::string> Classifier::loadClassNames(const std::string& path) {
	std::vector<std::string> class_names;
	std::ifstream infile(path);
	if (infile.is_open()) {
		std::string line;
		while (getline(infile, line)) {
			class_names.emplace_back(line);
		}

		infile.close();
	}
	else {
		std::cerr << "Errir loading the class names!\n";
	}
	return class_names;
}

std::string Classifier::classify(const cv::Mat img) {

	torch::Device device(device_type);

	try 
	{
		// ��ȡͼƬ
		cv::Mat image = img.clone();
		cv::Mat image_transfomed;
		cv::resize(image, image_transfomed, cv::Size(224, 224));
		cv::cvtColor(image_transfomed, image_transfomed, cv::COLOR_BGR2RGB);
		// ͼ��ת��ΪTensor
		torch::Tensor tensor_image = torch::from_blob(image_transfomed.data, { image_transfomed.rows, image_transfomed.cols,3 }, torch::kByte);
		tensor_image = tensor_image.permute({ 2,0,1 });
		tensor_image = tensor_image.toType(torch::kFloat);
		tensor_image = tensor_image.div(255);
		tensor_image = tensor_image.unsqueeze(0);
		tensor_image = tensor_image.to(device);

		// ����ǰ�����
		// Execute the model and turn its output into a tensor.
		torch::NoGradGuard no_grad;
		at::Tensor output = module.forward({ tensor_image }).toTensor();
		torch::Tensor prob = torch::softmax(output, 1);
		auto predict = torch::max(output, 1);
		int index = std::get<1>(predict).item<int>();

		return labels_[index];
	}
	catch (const std::exception& e) { // caught by reference to base
		return e.what();
	}
	
}


