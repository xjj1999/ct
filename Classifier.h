#pragma once
#include <opencv2/opencv.hpp>
#undef slots
#include <torch/torch.h>
#define slots Q_SLOTS
#include <torch/script.h>

class Classifier
{

public:

    Classifier(const std::string& model_file, const std::string& label_file, torch::DeviceType device_type);
    std::vector<std::string> loadClassNames(const std::string& path);
    std::string classify(const cv::Mat img);

private:

    std::vector<std::string> labels_;
    torch::jit::script::Module module;
    torch::DeviceType device_type;

};
