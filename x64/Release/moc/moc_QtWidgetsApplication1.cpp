/****************************************************************************
** Meta object code from reading C++ file 'QtWidgetsApplication1.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.10)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../QtWidgetsApplication1.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QtWidgetsApplication1.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.10. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TorchInferDiag_t {
    QByteArrayData data[12];
    char stringdata0[203];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TorchInferDiag_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TorchInferDiag_t qt_meta_stringdata_TorchInferDiag = {
    {
QT_MOC_LITERAL(0, 0, 14), // "TorchInferDiag"
QT_MOC_LITERAL(1, 15, 16), // "ClickInferButton"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 18), // "ClickLoadImgButton"
QT_MOC_LITERAL(4, 52, 21), // "ClickLoadOutFolderBtn"
QT_MOC_LITERAL(5, 74, 18), // "ClickLoadexeButton"
QT_MOC_LITERAL(6, 93, 17), // "ClickLoadinButton"
QT_MOC_LITERAL(7, 111, 21), // "ClickOpenFolderButton"
QT_MOC_LITERAL(8, 133, 20), // "taskComboIndexChange"
QT_MOC_LITERAL(9, 154, 4), // "text"
QT_MOC_LITERAL(10, 159, 22), // "deviceComboIndexChange"
QT_MOC_LITERAL(11, 182, 20) // "sizeComboIndexChange"

    },
    "TorchInferDiag\0ClickInferButton\0\0"
    "ClickLoadImgButton\0ClickLoadOutFolderBtn\0"
    "ClickLoadexeButton\0ClickLoadinButton\0"
    "ClickOpenFolderButton\0taskComboIndexChange\0"
    "text\0deviceComboIndexChange\0"
    "sizeComboIndexChange"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TorchInferDiag[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x0a /* Public */,
       3,    0,   60,    2, 0x0a /* Public */,
       4,    0,   61,    2, 0x0a /* Public */,
       5,    0,   62,    2, 0x0a /* Public */,
       6,    0,   63,    2, 0x0a /* Public */,
       7,    0,   64,    2, 0x0a /* Public */,
       8,    1,   65,    2, 0x0a /* Public */,
      10,    1,   68,    2, 0x0a /* Public */,
      11,    1,   71,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Int, QMetaType::QString,    9,
    QMetaType::Int, QMetaType::QString,    9,
    QMetaType::Int, QMetaType::QString,    9,

       0        // eod
};

void TorchInferDiag::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TorchInferDiag *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ClickInferButton(); break;
        case 1: _t->ClickLoadImgButton(); break;
        case 2: _t->ClickLoadOutFolderBtn(); break;
        case 3: _t->ClickLoadexeButton(); break;
        case 4: _t->ClickLoadinButton(); break;
        case 5: _t->ClickOpenFolderButton(); break;
        case 6: { int _r = _t->taskComboIndexChange((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 7: { int _r = _t->deviceComboIndexChange((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 8: { int _r = _t->sizeComboIndexChange((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TorchInferDiag::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_TorchInferDiag.data,
    qt_meta_data_TorchInferDiag,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TorchInferDiag::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TorchInferDiag::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TorchInferDiag.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int TorchInferDiag::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
