/********************************************************************************
** Form generated from reading UI file 'QtWidgetsApplication1.ui'
**
** Created by: Qt User Interface Compiler version 5.12.10
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTWIDGETSAPPLICATION1_H
#define UI_QTWIDGETSAPPLICATION1_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>

QT_BEGIN_NAMESPACE

class Ui_TorchInferDiagClass
{
public:

    void setupUi(QDialog *TorchInferDiagClass)
    {
        if (TorchInferDiagClass->objectName().isEmpty())
            TorchInferDiagClass->setObjectName(QString::fromUtf8("TorchInferDiagClass"));
        TorchInferDiagClass->resize(600, 400);

        retranslateUi(TorchInferDiagClass);

        QMetaObject::connectSlotsByName(TorchInferDiagClass);
    } // setupUi

    void retranslateUi(QDialog *TorchInferDiagClass)
    {
        TorchInferDiagClass->setWindowTitle(QApplication::translate("TorchInferDiagClass", "TorchInferDiag", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TorchInferDiagClass: public Ui_TorchInferDiagClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTWIDGETSAPPLICATION1_H
