#include "Segmentor.h"

Segmentor::Segmentor(const std::string& model_file, int num_class, torch::DeviceType dt) {

    // set device type and device, cpu inference or gpu inference
    device_type = dt;
    torch::Device device(device_type);

    /* Load Module from model file path */
    module = torch::jit::load(model_file, device);
    module.eval();
	num_cls = num_class;
	colorMap = new cv::Vec3b[num_cls]{
		cv::Vec3b(0, 0, 0),
		cv::Vec3b(244, 35,232),
		cv::Vec3b(70, 70, 70),
		cv::Vec3b(102,102,156),
		cv::Vec3b(190,153,153),

		cv::Vec3b(153,153,153),
		cv::Vec3b(250,170, 30),
		cv::Vec3b(220,220,  0),
		cv::Vec3b(107,142, 35),
		cv::Vec3b(152,251,152),

		cv::Vec3b(70,130,180),
		cv::Vec3b(220, 20, 60),
		cv::Vec3b(255,  0,  0),
		cv::Vec3b(0,  0,142),
		cv::Vec3b(0,  0, 70),

		cv::Vec3b(0, 60,100),
		cv::Vec3b(0, 80,100),
		cv::Vec3b(0,  0,230),
		cv::Vec3b(119, 11, 32),
		cv::Vec3b(0,  0,  0)
	};
}

torch::Tensor Segmentor::process(cv::Mat& image, int img_size)
{

	torch::Device device(device_type);
	  //首先对输入的图片进行处理
	cv::cvtColor(image, image, cv::COLOR_BGR2RGB);// bgr -> rgb
	cv::Mat img_float;
	// image.convertTo(img_float, CV_32F, 1.0 / 255);//归一化到[0,1]区间,
	cv::resize(image, img_float, cv::Size(img_size, img_size));

	std::vector<int64_t> dims = { 1, img_size, img_size, 3 };
	torch::Tensor img_var = torch::from_blob(img_float.data, dims, torch::kByte).to(device);//将图像转化成张量

	img_var = img_var.permute({ 0,3,1,2 });//将张量的参数顺序转化为 torch输入的格式 1,3,384,384
	img_var = img_var.toType(torch::kFloat);
	img_var = img_var.div(255);

	return img_var;

}

cv::Mat Segmentor::get_mask(const cv::Mat& bgr, int img_size) {

	torch::Device device(device_type);
	// 读取图片
	cv::Mat img_input = bgr.clone();
	//对图片进行处理，得到张量
	torch::Tensor tensor_image = process(img_input, img_size);
	torch::NoGradGuard no_grad;
	torch::Tensor result = module.forward({ tensor_image }).toTensor();  //前向传播获取结果，还是tensor类型

	result = result.argmax(1);//找出每个点概率最大的一个
	result = result.squeeze();//删除一个维度   
	result = result.to(torch::kU8);//.mul(100)这里是为了让分割的区域更明显,但是不需要加，因为后面使用了lut的方法可以使不同的mask显示不同颜色
	result = result.to(torch::kCPU);
	cv::Mat pts_mat(cv::Size(img_size, img_size), CV_8U, result.data_ptr());//新建一个矩阵，用于保存数据，将tensor的数据转移到这里面

	cv::Mat coloredImg(pts_mat.rows, pts_mat.cols, CV_8UC3);

	size_t min_label = 255, max_label = 0;
	for (size_t x = 0; x < coloredImg.rows; ++x)
	{
		for (size_t y = 0; y < coloredImg.cols; ++y)
		{
			uint8_t label = pts_mat.at<uint8_t>(x, y);

			if (label < 20)
			{
				coloredImg.at<cv::Vec3b>(x, y) = colorMap[label];
			}
			else
			{
				coloredImg.at<cv::Vec3b>(x, y) = cv::Vec3b(0, 0, 0);
			}
			min_label = label < min_label ? label : min_label;
			max_label = label > max_label ? label : max_label;
		}
	}

	return coloredImg;

}

cv::Mat Segmentor::draw_mask(const cv::Mat& image, cv::Mat& mask, float alpha) {

	cv::Mat new_img = image.clone();
	cv::Mat img_resize, out;
	cv::resize(new_img, img_resize, cv::Size(384, 384));

	cv::addWeighted(mask, alpha, img_resize, 1-alpha, 0.0, out);

	return out;
}

void Segmentor::processFolder(std::vector<std::string> in_vector, std::string inFolder, std::string outFolder)
{
	cv::Mat img, mask, dst;
	for (int i = 0; i < in_vector.size(); ++i)
	{
		std::string path = inFolder + in_vector[i];

		img = cv::imread(path, cv::ImreadModes::IMREAD_COLOR);

		mask = get_mask(img, 384);
		dst = draw_mask(img, mask, 0.4);

		std::string out_path = outFolder + std::to_string(i) + ".png";
		cv::imwrite(out_path, dst);

	}
	img.release();
	mask.release();
	dst.release();

}