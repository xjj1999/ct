#pragma once
#include <opencv2/opencv.hpp>
#undef slots
#include <torch/torch.h>
#define slots Q_SLOTS
#include <torch/script.h>
#include <vector>

class Generator
{
public:
    Generator(const std::string& model_file, torch::DeviceType device_type);
    cv::Mat Generator::get_raw_image(const char* path, int width, int height);
    cv::Mat Generator::generate(const cv::Mat& src, const std::string path);
    void Generator::processFolder(std::vector<std::string> vec, std::string inFolder, std::string outFolder, int width, int height,float ratio);
    void Generator::saveRawData(const char* path, const torch::Tensor src);
    torch::Tensor Generator::generate_raw(const cv::Mat& img,float ratio);

private:
    torch::jit::script::Module module;
    torch::DeviceType device_type;
};

