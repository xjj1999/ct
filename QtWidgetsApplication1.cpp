#include "QtWidgetsApplication1.h"
#include <QtWidgets/QApplication>
#include <QtWidgets/QSpinBox> 
#include <QtWidgets/QSlider> 
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QLineEdit>
#include <QMessageBox>	
#include <QFileDialog>
#include <opencv2/opencv.hpp>
#undef slots
#include <torch/torch.h>
#define slots Q_SLOTS
#include <torch/script.h>
#include <iostream>
#include <memory>
#include <QTime>
#include <vector>
using namespace cv;
using namespace std;

TorchInferDiag::TorchInferDiag(QWidget *parent)
    : QDialog(parent)
{	
	width = 256;
	height=256;
	QPalette text_color;
	text_color.setColor(QPalette::WindowText, QColor(192, 192, 192, 255));

	QVBoxLayout* vImglayout = new QVBoxLayout;
	imageLabel = new QLabel;

	QImage* img = new QImage;
	QString icon_path = "./resource/classification_model/gold_fish.jpg";
	current_image = cv::imread(icon_path.toStdString(), cv::ImreadModes::IMREAD_COLOR);
	img->load(QString(icon_path));
	//img->load(QString("1.jpg"));
	QImage scaledimg;
	scaledimg = img->scaled(imageLabel->width(), imageLabel->height(), Qt::KeepAspectRatio);
	imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	imageLabel->setScaledContents(true);
	imageLabel->setPixmap(QPixmap::fromImage(scaledimg));
	imageLabel->setAlignment(Qt::AlignCenter);
	imageLabel->setMargin(1);

	QHBoxLayout* imgPathLayout = new QHBoxLayout;
	QLabel* imgPathLabel = new QLabel();
	imgPathLabel->setPalette(text_color);
	imgPathLabel->setText("Image Path:");
	imgPathEdit = new QLineEdit();
	imgPathEdit->setPlaceholderText("Please enter image path ...");
	loadImgBtn = new QPushButton();
	loadImgBtn->setText("Load Image");
	connect(loadImgBtn, SIGNAL(clicked()), this, SLOT(ClickLoadImgButton()));

	QHBoxLayout* outFolderPathLayout = new QHBoxLayout;
	QLabel* outFolderPathLabel = new QLabel();
	outFolderPathLabel->setPalette(text_color);
	outFolderPathLabel->setText("Output Path:");
	outFolderPathEdit = new QLineEdit();
	outFolderPathEdit->setPlaceholderText("Please enter output folder path ...");
	loadOutFolderBtn = new QPushButton();
	loadOutFolderBtn->setText("Load Output Folder");
	connect(loadOutFolderBtn, SIGNAL(clicked()), this, SLOT(ClickLoadOutFolderBtn()));


	QHBoxLayout* exePathLayout = new QHBoxLayout;
	QLabel* exePathLabel = new QLabel();
	exePathLabel->setPalette(text_color);
	exePathLabel->setText("exe Path:");
	exePathEdit = new QLineEdit();
	exePathEdit->setPlaceholderText("Please enter exe path ...");
	loadexeBtn = new QPushButton();
	loadexeBtn->setText("Load Exe");
	connect(loadexeBtn, SIGNAL(clicked()), this, SLOT(ClickLoadexeButton()));

	QHBoxLayout* inPathLayout = new QHBoxLayout;
	QLabel* inPathLabel = new QLabel();
	inPathLabel->setPalette(text_color);
	inPathLabel->setText("In Path:");
	inPathEdit = new QLineEdit();
	inPathEdit->setPlaceholderText("Please enter in path ...");
	loadinBtn = new QPushButton();
	loadinBtn->setText("Load in");
	connect(loadinBtn, SIGNAL(clicked()), this, SLOT(ClickLoadinButton()));

	imgPathLayout->addWidget(imgPathLabel);
	imgPathLayout->addWidget(imgPathEdit);
	imgPathLayout->addWidget(loadImgBtn);

	outFolderPathLayout->addWidget(outFolderPathLabel);
	outFolderPathLayout->addWidget(outFolderPathEdit);
	outFolderPathLayout->addWidget(loadOutFolderBtn);

	exePathLayout->addWidget(exePathLabel);
	exePathLayout->addWidget(exePathEdit);
	exePathLayout->addWidget(loadexeBtn);

	inPathLayout->addWidget(inPathLabel);
	inPathLayout->addWidget(inPathEdit);
	inPathLayout->addWidget(loadinBtn);

	vImglayout->addWidget(imageLabel);
	vImglayout->addLayout(imgPathLayout);
	vImglayout->addLayout(outFolderPathLayout);
	vImglayout->addLayout(exePathLayout);
	vImglayout->addLayout(inPathLayout);
	vImglayout->setStretchFactor(imageLabel, 9);
	vImglayout->setStretchFactor(imgPathLayout, 1);
	vImglayout->setStretchFactor(outFolderPathLayout, 1);
	vImglayout->setStretchFactor(exePathLayout, 1);
	vImglayout->setStretchFactor(inPathLayout, 1);

	QLabel* titleLabel = new QLabel();
	QFont font;//实例化QFont对象
	font.setPixelSize(25);//文字像素大小
	titleLabel->setFont(font);
	titleLabel->setPalette(text_color);
	titleLabel->setAlignment(Qt::AlignCenter);
	titleLabel->setStyleSheet("QLabel{border:1px solid rgb(192, 192, 192);}");
	titleLabel->setText("Basic Setting");

	QVBoxLayout* vlayout = new QVBoxLayout;

	QGroupBox* groupBox = new QGroupBox(QObject::tr("Load Model"));


	QGridLayout* pLayout = new QGridLayout();
	QLabel* taskComboLabel = new QLabel();
	taskComboLabel->setPalette(text_color);
	taskComboLabel->setText("Task:");
	QStringList QList;//停止位
	QList.clear();
	QList << QObject::tr("Classification") << QObject::tr("Detection") << QObject::tr("Segmentation") << QObject::tr("Pix2Pix")<<QObject::tr("Latest_net");
	taskCombo = new QComboBox;
	taskCombo->clear();
	taskCombo->addItems(QList);
	taskCombo->setCurrentIndex(0);
	connect(taskCombo, SIGNAL(currentIndexChanged(const QString)), this, SLOT(taskComboIndexChange(const QString)));



	QLabel* deviceComboLabel = new QLabel();
	deviceComboLabel->setPalette(text_color);
	deviceComboLabel->setText("Device:");
	QStringList QDeviceList;//停止位
	QDeviceList.clear();
	QDeviceList << QObject::tr("CPU") << QObject::tr("GPU");
	deviceCombo = new QComboBox;
	deviceCombo->addItems(QDeviceList);
	deviceCombo->setCurrentIndex(1);
	connect(deviceCombo, SIGNAL(currentIndexChanged(const QString)), this, SLOT(deviceComboIndexChange(const QString)));


	QLabel* sizeComboLabel = new QLabel();
	sizeComboLabel->setPalette(text_color);
	sizeComboLabel->setText("Size");
	QStringList QSizelist;
	QSizelist.clear();
	QSizelist << QObject::tr("256*256") << QObject::tr("1024*768");
	sizeCombo = new QComboBox;
	sizeCombo->addItems(QSizelist);
	sizeCombo->setCurrentIndex(0);
	connect(sizeCombo, SIGNAL(currentIndexChanged(const QString)), this, SLOT(sizeComboIndexChange(const QString)));




	QLabel* weightPathLabel = new QLabel();
	weightPathLabel->setPalette(text_color);
	weightPathLabel->setText("Weight Path:");
	ratioEdit = new QLineEdit();
	ratioEdit->setPlaceholderText("Please enter ratio ...");

	QLabel* ratiolabel = new QLabel();
	ratiolabel->setPalette(text_color);
	ratiolabel->setText("Ratio:");
	weightPathEdit = new QTextEdit();
	weightPathEdit->setPlaceholderText("Please enter weight path ...");


	pLayout->addWidget(taskComboLabel, 0, 0, 1, 1);
	pLayout->addWidget(taskCombo, 0, 1, 1, 1);
	pLayout->addWidget(deviceComboLabel, 1, 0, 1, 1);
	pLayout->addWidget(deviceCombo, 1, 1, 1, 1);
	pLayout->addWidget(sizeComboLabel, 2, 0, 1, 1);
	pLayout->addWidget(sizeCombo, 2, 1, 1, 1);
	pLayout->addWidget(ratiolabel, 3, 0, 1, 1);
	pLayout->addWidget(ratioEdit, 3, 1, 1, 1);
	pLayout->addWidget(weightPathLabel, 4, 0, 1, 1);
	pLayout->addWidget(weightPathEdit, 4, 1, 1, 1);
	groupBox->setPalette(text_color);
	groupBox->setLayout(pLayout);

	inferBtn = new QPushButton();
	inferBtn->setText("Infer Model");
	connect(inferBtn, SIGNAL(clicked()), this, SLOT(ClickInferButton()));

	openFolderBtn = new QPushButton();
	openFolderBtn->setText("Open Folder");
	connect(openFolderBtn, SIGNAL(clicked()), this, SLOT(ClickOpenFolderButton()));

	QGroupBox* resultGroupbox = new QGroupBox(QObject::tr("Result"));
	QVBoxLayout* vResultboxlayout = new QVBoxLayout;
	resultLabel = new QLabel();
	resultLabel->setPalette(text_color);
	resultLabel->setText("Result is below");
	vResultboxlayout->addWidget(resultLabel);
	resultGroupbox->setPalette(text_color);
	resultGroupbox->setLayout(vResultboxlayout);

	vlayout->addWidget(titleLabel);
	vlayout->addWidget(groupBox);
	vlayout->addWidget(inferBtn, Qt::AlignRight);
	vlayout->addWidget(openFolderBtn, Qt::AlignRight);
	vlayout->addWidget(resultGroupbox);
	vlayout->setStretchFactor(titleLabel, 1);
	vlayout->setStretchFactor(groupBox, 2);
	vlayout->setStretchFactor(inferBtn, 1);
	vlayout->setStretchFactor(openFolderBtn, 1);
	vlayout->setStretchFactor(resultGroupbox, 4);

	QHBoxLayout* layout = new QHBoxLayout;

	layout->addLayout(vImglayout, 3);
	layout->addLayout(vlayout, 1);
	//layout->setStretchFactor(imageLabel, 2);
	//layout->setStretchFactor(vlayout, 1);
	layout->setMargin(1);
	setFixedSize(1200, 720);
	setWindowOpacity(0.95);
	setLayout(layout);

	//设置Device类型
	//torch::Device device(device_type);
	int current_device_index = deviceCombo->currentIndex();
	if (current_device_index == 1)
	{
		device_type = torch::kCUDA;
		current_device = "GPU\n";
	}
	else
	{
		device_type = torch::kCPU;  //torch::kCUDA  and torch::kCPU
		current_device = "CPU\n";
	}

	imagenet_class_names_path = "./resource/classification_model/imagenet.txt";
	coco_class_names_path = "./resource/detection_model/coco.names";
	cls_weight_path = "./resource/classification_model/traces_script_module_cpu.pt";
	det_weight_path = "./resource/detection_model/yolov5s.torchscript_cuda.pt";
	seg_weight_path = "./resource/seg_model/espnetv2_s_2.0_pascal_384x384_torchscript.pt";
	pix2pix_weight_path = "./resource/pix2pix_model/pix2pix_scatter.pt";
	latest_weight_path = "./resource/latest_net_G.pt";
	///*imagenet_class_names_path = "./x64/Release/resource/classification_model/imagenet.txt";
	//coco_class_names_path = "./x64/Release/resource/detection_model/coco.names";
	//cls_weight_path = "./x64/Release/resource/classification_model/traces_script_module_cpu.pt";
	//det_weight_path = "./x64/Release/resource/detection_model/yolov5s.torchscript_cuda.pt";
	//seg_weight_path = "./x64/Release/resource/seg_model/espnetv2_s_2.0_pascal_384x384_torchscript.pt";
	//pix2pix_weight_path = "./x64/Release/resource/pix2pix_model/pix2pix_scatter.pt";*/

	//cls_module = new Classifier(cls_weight_path.toStdString(), imagenet_class_names_path.toStdString(), device_type);
	det_module = new Detector(det_weight_path.toStdString(), coco_class_names_path.toStdString(), device_type);
	seg_module = new Segmentor(seg_weight_path.toStdString(), 20, device_type);
	pix2pix_module = new Generator(pix2pix_weight_path.toStdString(), device_type);
	latest_module= new Generator(latest_weight_path.toStdString(), device_type);
	//////设置Device类型
	//weightPathEdit->setText(cls_weight_path);

	//QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + QString::fromStdString(cls_module->classify(current_image));
	//resultLabel->setText(result_label_qstring);
}










QStringList getRawNames(const QString& path)
{
	QDir dir(path);
	QStringList nameFilters;
	nameFilters << "*.raw";
	QStringList files = dir.entryList(nameFilters, QDir::Files | QDir::Readable, QDir::Name);
	return files;
}

QStringList getImgNames(const QString& path)
{
	QDir dir(path);
	QStringList nameFilters;
	nameFilters << "*.jpg" << "*.png" << "*.jpeg" << "*.bmp";
	QStringList files = dir.entryList(nameFilters, QDir::Files | QDir::Readable, QDir::Name);
	return files;
}
void TorchInferDiag::ClickLoadOutFolderBtn()
{
	//what you want to do
	QString outDirPath;

	outDirPath = QFileDialog::getExistingDirectory(
		this, "choose src Directory",
		"./");

	if (outDirPath.isEmpty())
	{
		return;
	}
	else
	{
		qDebug() << "outDirPath=" << outDirPath;
		outDirPath += "/";
		outFolderPathEdit->setText(outDirPath);

	}
}
void TorchInferDiag::ClickInferButton()
{
	//设置Device类型
	torch::Device device(device_type);
	// get current task index
	int current_infer_index = taskCombo->currentIndex();
	QImage qImg;

	srcDirPath = imgPathEdit->text();
	QFileInfo f(srcDirPath);
	bool is_folder = f.isDir();
	resultLabel->setText(QString::number(is_folder));

	std::vector<std::string> vec;

	if (current_infer_index == 2) {

		if (is_folder)
		{
			QString outputFolder = outFolderPathEdit->text();

			if (outputFolder.isEmpty())
			{
				return;
			}
			else
			{
				vec.clear();
				QStringList img_path_list = getImgNames(srcDirPath);
				foreach(QString str_in, img_path_list) {
					vec.push_back(str_in.toStdString());
				}

				QTime startTime = QTime::currentTime();
				seg_module->processFolder(vec, srcDirPath.toStdString(), outputFolder.toStdString());
				QTime stopTime = QTime::currentTime();
				float elapsed = startTime.msecsTo(stopTime);

				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + QString::number(elapsed);
				resultLabel->setText(result_label_qstring);

			}
		}
		else
		{
			if (!current_image.empty())
			{
				cv::Mat image = current_image.clone();
				QTime startTime = QTime::currentTime();
				cv::Mat mask = seg_module->get_mask(image, 384);
				cv::Mat dst = seg_module->draw_mask(image, mask, 0.4);
				QTime stopTime = QTime::currentTime();
				float elapsed = startTime.msecsTo(stopTime);

				cv::cvtColor(dst, dst, cv::COLOR_BGR2RGB);
				qImg = QImage((const unsigned char*)(dst.data), dst.cols, dst.rows, dst.step, QImage::Format_RGB888);
				QImage scaledimg;
				scaledimg = qImg.scaled(imageLabel->width(), imageLabel->height(), Qt::KeepAspectRatio);
				imageLabel->setPixmap(QPixmap::fromImage(qImg));
				imageLabel->setMargin(1);

				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + QString::number(elapsed);
				resultLabel->setText(result_label_qstring);
			}
			else
			{
				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + "No image to infer";
				//QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Result:" + QString::fromStdString(cls_module->classify(current_image));
				resultLabel->setText(result_label_qstring);
			}
		}
	}
	else if (current_infer_index == 1)
	{
		if (is_folder)
		{
			QString outputFolder = outFolderPathEdit->text();

			if (outputFolder.isEmpty())
			{
				return;
			}
			else
			{
				vec.clear();
				QStringList img_path_list = getImgNames(srcDirPath);
				foreach(QString str_in, img_path_list) {
					vec.push_back(str_in.toStdString());
				}

				QTime startTime = QTime::currentTime();
				det_module->processFolder(vec, srcDirPath.toStdString(), outputFolder.toStdString());
				QTime stopTime = QTime::currentTime();
				float elapsed = startTime.msecsTo(stopTime);

				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + QString::number(elapsed);
				resultLabel->setText(result_label_qstring);

			}
		}
		else
		{
			if (!current_image.empty())
			{
				cv::Mat image = current_image.clone();
				auto result = det_module->detect(current_image);

				image = det_module->draw_objects(current_image, result);

				cv::cvtColor(image, image, cv::COLOR_BGR2RGB);
				qImg = QImage((const unsigned char*)(image.data), image.cols, image.rows, image.step, QImage::Format_RGB888);
				QImage scaledimg;
				scaledimg = qImg.scaled(imageLabel->width(), imageLabel->height(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
				imageLabel->setPixmap(QPixmap::fromImage(qImg));
				imageLabel->setMargin(1);

				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + "Result showed on image";
				resultLabel->setText(result_label_qstring);
			}
			else
			{
				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + "No image to infer";
				//QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Result:" + QString::fromStdString(cls_module->classify(current_image));
				resultLabel->setText(result_label_qstring);
			}
		}
	}
	else if (current_infer_index == 0)
	{
		if (is_folder)
		{
			resultLabel->setText("not support folder inference");
		}
		else
		{
			if (!current_image.empty())
			{
				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + QString::fromStdString(cls_module->classify(current_image));
				//QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Result:" + QString::fromStdString(cls_module->classify(current_image));
				resultLabel->setText(result_label_qstring);
			}
			else
			{
				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + "No image to infer";
				//QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Result:" + QString::fromStdString(cls_module->classify(current_image));
				resultLabel->setText(result_label_qstring);
			}
		}
	}
	else if (current_infer_index == 3)
	{
		if (is_folder)
		{
			QString outputFolder = outFolderPathEdit->text();

			if (outputFolder.isEmpty())
			{
				return;
			}
			else
			{
				vec.clear();
				QStringList img_path_list = getRawNames(srcDirPath);
				foreach(QString str_in, img_path_list) {
					vec.push_back(str_in.toStdString());
				}
				QTime startTime = QTime::currentTime();
				pix2pix_module->processFolder(vec, srcDirPath.toStdString(), outputFolder.toStdString(),width,height,1);
				QTime stopTime = QTime::currentTime();
				float elapsed = startTime.msecsTo(stopTime);
				//QString str_join = img_path_list.join(",");
				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + QString::number(elapsed);
				resultLabel->setText(result_label_qstring);
			}
		}
		else
		{
			QString outputFolder = outFolderPathEdit->text();
			if (!current_image.empty())
			{
				QTime startTime = QTime::currentTime();
				std::string out_path = outputFolder.toStdString()+ std::to_string(0) + ".raw";
				cv::Mat temp = pix2pix_module->generate(current_image, out_path);
				
				/*cv::Mat img_r = pix2pix_module->get_raw_image(imgPathEdit->displayText().toStdString().c_str(), width, height);

				torch::Tensor temp_img=pix2pix_module->generate_raw(img_r);*/
				
				QTime stopTime = QTime::currentTime();
				float elapsed = startTime.msecsTo(stopTime);
				QImage qImg, scaledimg;
				qImg = QImage((const unsigned char*)(temp.data), temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
				scaledimg = qImg.scaled(imageLabel->width(), imageLabel->height(), Qt::KeepAspectRatio);

				imageLabel->setPixmap(QPixmap::fromImage(qImg));
				imageLabel->setMargin(1);

				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + QString::number(elapsed);
				resultLabel->setText(result_label_qstring);
			}
			else
			{
				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + "No image to infer";
				//QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Result:" + QString::fromStdString(cls_module->classify(current_image));
				resultLabel->setText(result_label_qstring);
			}
		}
	}
	else if (current_infer_index == 4) 
	{
		if (is_folder)
		{
			QString outputFolder = outFolderPathEdit->text();

			if (outputFolder.isEmpty())
			{
				return;
			}
			else
			{
				float ratio = ratioEdit->text().toFloat();
				vec.clear();
				QStringList img_path_list = getRawNames(srcDirPath);
				foreach(QString str_in, img_path_list) {
				vec.push_back(str_in.toStdString());
			}
				QTime startTime = QTime::currentTime();
				latest_module->processFolder(vec, srcDirPath.toStdString(), outputFolder.toStdString(), width, height,ratio);
				String command_rb = exePathEdit->text().toStdString()+" "+inPathEdit->text().toStdString();
				QTime stopTime = QTime::currentTime();
				system(command_rb.c_str());
				float elapsed = startTime.msecsTo(stopTime);
				//QString str_join = img_path_list.join(",");
				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + QString::number(elapsed);
				resultLabel->setText(result_label_qstring);
			}
		}
		else
		{
			QString outputFolder = outFolderPathEdit->text();
			if (!current_image.empty())
			{
				QTime startTime = QTime::currentTime();
				std::string out_path = outputFolder.toStdString() + std::to_string(0) + ".raw";
				cv::Mat temp = latest_module->generate(current_image, out_path);

				/*cv::Mat img_r = pix2pix_module->get_raw_image(imgPathEdit->displayText().toStdString().c_str(), width, height);

				torch::Tensor temp_img=pix2pix_module->generate_raw(img_r);*/

				QTime stopTime = QTime::currentTime();
				float elapsed = startTime.msecsTo(stopTime);
				QImage qImg, scaledimg;
				qImg = QImage((const unsigned char*)(temp.data), temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
				scaledimg = qImg.scaled(imageLabel->width(), imageLabel->height(), Qt::KeepAspectRatio);

				imageLabel->setPixmap(QPixmap::fromImage(qImg));
				imageLabel->setMargin(1);
	
				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + QString::number(elapsed);
				resultLabel->setText(result_label_qstring);
			}
			else
			{
				QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + "No image to infer";
				//QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Result:" + QString::fromStdString(cls_module->classify(current_image));
				resultLabel->setText(result_label_qstring);
			}
		}
	}
}

void TorchInferDiag::ClickLoadImgButton()
{
	//what you want to do
	QString fileName;
	if (taskCombo->currentIndex() == 3|| taskCombo->currentIndex() == 4) {
		fileName = QFileDialog::getOpenFileName(this, tr("选择一张图片"), "./resource", tr("Image Files (*.raw)"));
		imgPathEdit->setText(fileName);
	}
	else
	{
		fileName = QFileDialog::getOpenFileName(this, tr("选择一张图片"), "./resource", tr("Image Files (*.png *.jpg  *.jpeg *.bmp)"));
		imgPathEdit->setText(fileName);
	}

	torch::Device device(device_type);

	if (taskCombo->currentIndex() == 3|| taskCombo->currentIndex() == 4) {

		//int height = 256;//原始图像的高
		//int width = 256;//原始图像的宽

		cv::Mat img, temp, gray_temp;
		img = pix2pix_module->get_raw_image(fileName.toStdString().c_str(), width, height);
		if (!img.empty())
		{
			current_image = img.clone();

			double minv = 0.0, maxv = 0.0;
			double* minp = &minv;
			double* maxp = &maxv;
			minMaxIdx(img, minp, maxp);//找到最大和最小的像素值

			float max_value_pre = *maxp;
			float min_value_pre = *minp;
			temp = img - min_value_pre;
			temp = temp / max_value_pre;
			temp = temp * 255;

			temp.convertTo(gray_temp, CV_8UC1);

			//cv::cvtColor(temp, temp, CV_GRAY2RGB);//GRAY convert to RGB
			QImage qImg, scaledimg;
			qImg = QImage((const unsigned char*)(gray_temp.data), gray_temp.cols, gray_temp.rows, gray_temp.step, QImage::Format_Indexed8);
			scaledimg = qImg.scaled(imageLabel->width(), imageLabel->height(), Qt::KeepAspectRatio);
			imageLabel->setPixmap(QPixmap::fromImage(qImg));
			imageLabel->setMargin(1);
		}
		else
		{
			QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + "No image selected";
			resultLabel->setText(result_label_qstring);
		}
	}
	else
	{
		current_image = cv::imread(fileName.toStdString(), cv::ImreadModes::IMREAD_COLOR);
		if (!current_image.empty())
		{
			cv::Mat dst;
			cv::cvtColor(current_image, dst, cv::COLOR_BGR2RGB);
			QImage qImg = QImage((const unsigned char*)(dst.data), dst.cols, dst.rows, dst.step, QImage::Format_RGB888).scaled(imageLabel->width(), imageLabel->height(), Qt::KeepAspectRatio);
			imageLabel->setPixmap(QPixmap::fromImage(qImg));
			imageLabel->setMargin(1);
		}
		else
		{
			QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + "No image selected";
			resultLabel->setText(result_label_qstring);
		}

	}
}

int TorchInferDiag::deviceComboIndexChange(const QString& text)
{
	int current_device_index = deviceCombo->currentIndex();
	if (current_device_index == 1)
	{
		device_type = torch::kCUDA;
		current_device = "GPU\n";
	}
	else
	{
		device_type = torch::kCPU;  //torch::kCUDA  and torch::kCPU
		current_device = "CPU\n";
	}

	//cls_module = new Classifier(cls_weight_path.toStdString(), imagenet_class_names_path.toStdString(), device_type);
	det_module = new Detector(det_weight_path.toStdString(), coco_class_names_path.toStdString(), device_type);
	seg_module = new Segmentor(seg_weight_path.toStdString(), 20, device_type);
	pix2pix_module = new Generator(pix2pix_weight_path.toStdString(), device_type);
	latest_module = new Generator(latest_weight_path.toStdString(), device_type);

	QString result_label_qstring = "Current Task: " + taskCombo->currentText() + "\n" + "Current Device:" + current_device + "Current Result:" + "";
	resultLabel->setText(result_label_qstring);

	return 0;
}

int TorchInferDiag::taskComboIndexChange(const QString& text)
{
	resultLabel->setText("Change to " + text + " task");
	torch::Device device(device_type);
	int current_infer_index = taskCombo->currentIndex();
	if (current_infer_index == 0)
	{
		weightPathEdit->setText(cls_weight_path);
	}
	else if (current_infer_index == 1)
	{
		weightPathEdit->setText(det_weight_path);
	}
	else if (current_infer_index == 2)
	{
		weightPathEdit->setText(seg_weight_path);
	}
	else if (current_infer_index == 3)
	{
		weightPathEdit->setText(pix2pix_weight_path);
	}
	else if (current_infer_index == 4)
	{
		weightPathEdit->setText(latest_weight_path);
	}

	return 0;
}

int TorchInferDiag::sizeComboIndexChange(const QString& text) {
	int current_infer_index = sizeCombo->currentIndex();
	if (current_infer_index == 0)
	{
		width = 256;
		height = 256;
	}
	else if (current_infer_index == 1)
	{
		width = 1024;
		height = 768;
	}
	
	return 0;
}
void TorchInferDiag::ClickOpenFolderButton()
{

	srcDirPath = QFileDialog::getExistingDirectory(
		this, "choose src Directory",
		"./");

	if (srcDirPath.isEmpty())
	{
		return;
	}
	else
	{
		qDebug() << "srcDirPath=" << srcDirPath;
		srcDirPath += "/";
		imgPathEdit->setText(srcDirPath);
	}

}
void TorchInferDiag::ClickLoadexeButton() {
	QString fileName;
	fileName = QFileDialog::getOpenFileName(this, tr("选择exe"), "./resource", tr("exe Files (*.exe)"));
	exePathEdit->setText(fileName);
}
void TorchInferDiag::ClickLoadinButton() {
	QString fileName;
	fileName = QFileDialog::getOpenFileName(this, tr("选择in"), "./resource", tr("in Files (*.in)"));
	inPathEdit->setText(fileName);
}