#pragma once
#include <opencv2/opencv.hpp>
#undef slots
#include <torch/torch.h>
#define slots Q_SLOTS
#include <torch/script.h>

class Segmentor
{

public:
    Segmentor(const std::string& model_file, int num_class, torch::DeviceType device_type);
	cv::Mat Segmentor::get_mask(const cv::Mat& bgr, int img_size);
    torch::Tensor Segmentor::process(cv::Mat& image, int img_size);
    cv::Mat draw_mask(const cv::Mat& image, cv::Mat& mask, float alpha);
    void processFolder(std::vector<std::string> in_vector, std::string inFolder, std::string outFolder);

private:
    torch::jit::script::Module module;
    torch::DeviceType device_type;
	// ��ɫӳ���ϵ
	cv::Vec3b* colorMap;
    int num_cls;


};

