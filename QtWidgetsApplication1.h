#pragma once


#include <QDebug>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QLabel>
#include "ui_QtWidgetsApplication1.h"
#undef slots
#include <torch/torch.h>
#define slots Q_SLOTS
#include <torch/script.h>
#include "Classifier.h"
#include "Detector.h"
#include "Segmentor.h"
#include "Generator.h"

class TorchInferDiag : public QDialog
{
	Q_OBJECT

public:
	TorchInferDiag(QWidget* parent = Q_NULLPTR);

public slots:
	void ClickInferButton();
	void ClickLoadImgButton();
	void ClickLoadOutFolderBtn();
	void ClickLoadexeButton();
	void ClickLoadinButton();

	void ClickOpenFolderButton();
	int taskComboIndexChange(const QString& text);
	int deviceComboIndexChange(const QString& text);
	int sizeComboIndexChange(const QString& text);

private:
	Ui::TorchInferDiagClass ui;

	QPushButton* inferBtn;
	QPushButton* openFolderBtn;
	QTextEdit* weightPathEdit;

	QLabel* imageLabel;
	QLineEdit* ratioEdit;

	QLineEdit* imgPathEdit;
	QPushButton* loadImgBtn
		;
	QLineEdit* outFolderPathEdit;
	QPushButton* loadOutFolderBtn;

	QLineEdit* exePathEdit;
	QPushButton* loadexeBtn
		;
	QLineEdit* inPathEdit;
	QPushButton* loadinBtn;

	QLabel* resultLabel;
	QComboBox* taskCombo;
	QComboBox* deviceCombo;
	
	QComboBox* sizeCombo;
	
	torch::jit::script::Module module;
	torch::DeviceType device_type;
	std::vector<std::string> imagenet_class_names;
	std::vector<std::string> coco_class_names;

	QString imagenet_class_names_path;
	QString coco_class_names_path;

	QString cls_weight_path;
	QString det_weight_path;
	QString seg_weight_path;
	QString pix2pix_weight_path;
	QString latest_weight_path;

	QString srcDirPath;
	QString exepath;
	QString inpath;

	QString current_device;

	QString result_show_string;
	torch::Tensor tensor_image;
	cv::Mat current_image;

	Classifier* cls_module;
	Detector* det_module;
	Segmentor* seg_module;
	Generator* pix2pix_module;
	Generator* latest_module;
	int width;
	int height;
};
