#include "QtWidgetsApplication1.h"
#include <QtWidgets/QApplication>
#include<stdio.h>
int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    TorchInferDiag w;

    w.setWindowTitle("Demo for some deep learning applications");
    printf("success");
    w.setWindowIcon(QIcon("./resource/icon.ico"));

    QPalette palette(w.palette());
    palette.setColor(QPalette::Background, QColor(32, 32, 32, 255));

    QFont font;
    font.setPointSize(10);

    w.setPalette(palette);
    w.setFont(font);
    w.show();
    return a.exec();
}
