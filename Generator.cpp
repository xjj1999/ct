#include "Generator.h"
#include <ppl.h>


Generator::Generator(const std::string& model_file, torch::DeviceType dt) {

	// set device type and device, cpu inference or gpu inference
	device_type = dt;
	torch::Device device(device_type);

	/* Load Module from model file path */
	module = torch::jit::load(model_file, device);
	module.eval();
	
}

cv::Mat Generator::get_raw_image(const char* path, int width, int height) {

	FILE* fp = NULL; //定义指针
	fp = fopen(path, "rb+");
	cv::Mat img, temp;

	if (fp != NULL)
	{
		float* data = (float*)malloc(width * height * sizeof(float)); //内存分配，new，malloc都行
		fread(data, sizeof(float), width * height, fp); //在缓存中读取数据
		
		int bufLen = width * height;  //定义长度
		img.create(height, width, CV_32FC1);//创建Mat
		memcpy(img.data, data, bufLen * sizeof(float)); //内存拷贝
	}
	fclose(fp);
	return img;

}

cv::Mat Generator::generate(const cv::Mat& img, const std::string path ) {

	torch::Device device(device_type);
	cv::Mat img_1 = cv::Mat(cv::Size(256, 256),img.type());
	cv::resize(img,img_1,img_1.size(), CV_INTER_AREA);
	double minv = 0.0, maxv = 0.0;
	double* minp = &minv;
	double* maxp = &maxv;
	minMaxIdx(img_1, minp, maxp);//找到最大和最小的像素值

	float max_value_pre = *maxp;

	torch::Tensor tensor_image = torch::from_blob(img_1.data, { img_1.rows, img_1.cols,1 }, torch::kFloat);
	tensor_image = tensor_image.permute({ 2,0,1 });
	tensor_image = tensor_image.toType(torch::kFloat);
	tensor_image = tensor_image.div(max_value_pre);

	tensor_image = tensor_image.unsqueeze(0);
	tensor_image = tensor_image.to(device);

	// 网络前向计算
	// Execute the model and turn its output into a tensor.
	torch::NoGradGuard no_grad;
	at::Tensor output = module.forward({ tensor_image }).toTensor();

	//torch::Device out_device(torch::kCPU);
	//auto out_tensor = output.to(out_device).squeeze().detach().to(at::kFloat);
	//auto accessor = out_tensor.accessor<float, 2>();
	//float data = accessor[0][0];

	auto out_tensor = output.squeeze().detach();
	out_tensor = out_tensor.sub_(output.min()).div_(output.max()).mul(255).to(torch::kU8);
	torch::Device out_device(torch::kCPU);
	out_tensor = out_tensor.to(out_device);


	auto out_tensor_1 = output.to(out_device).squeeze().detach().to(at::kFloat);
	saveRawData(path.c_str(),out_tensor_1);
	cv::Mat pts_mat(cv::Size(256, 256), CV_8U, out_tensor.data_ptr());
	cv::Mat temp;
	cv::cvtColor(pts_mat, temp, CV_GRAY2RGB);//GRAY convert to RGB

	return temp;
}


torch::Tensor Generator::generate_raw(const cv::Mat& img,float ratio) {

	torch::Device device(device_type);
	cv::Mat img_1 = cv::Mat(cv::Size(256, 256), img.type());
	cv::Mat result;
	result.create(cv::Size(img.cols, img.rows), img.type());
	cv::resize(img, img_1, img_1.size(), CV_INTER_AREA);
	double minv = 0.0, maxv = 0.0;
	double* minp = &minv;
	double* maxp = &maxv;
	minMaxIdx(img_1, minp, maxp);//找到最大和最小的像素值

	float max_value_pre = *maxp;

	torch::Tensor tensor_image = torch::from_blob(img_1.data, { img_1.rows, img_1.cols,1 }, torch::kFloat);
	tensor_image = tensor_image.permute({ 2,0,1 });
	tensor_image = tensor_image.toType(torch::kFloat);
	tensor_image = tensor_image.div(max_value_pre);
	tensor_image = tensor_image.unsqueeze(0);
	tensor_image = tensor_image.to(device);

	// 网络前向计算
	// Execute the model and turn its output into a tensor.
	torch::NoGradGuard no_grad;
	at::Tensor output = module.forward({ tensor_image }).toTensor();
	
	torch::Device out_device(torch::kCPU);
	//out_tensor = out_tensor.to(out_device);
	auto out_tensor = output.to(out_device).squeeze().detach().to(at::kFloat);
	cv::Mat img_C1;
	img_C1.create(cv::Size(img_1.cols, img_1.rows), img.type());
	memcpy(img_C1.data, out_tensor.data_ptr(), out_tensor.numel() * sizeof(torch::kFloat));
	cv::Mat img_C2;
	img_C2.create(cv::Size(img.cols, img.rows), img.type());
	cv::resize(img_C1, img_C2, img_C2.size(), CV_INTER_AREA);
	cv::absdiff(img ,img_C2,result);
	img_C1.release();
	img_C2.release();
	torch::Tensor out_tensor1 = torch::from_blob(result.data, { img.rows, img.cols,1 }, torch::kFloat);
	out_tensor1 = out_tensor1.mul(ratio);
	out_tensor1.permute({ 2,0,1 });
	auto out_tensor2 = out_tensor1.to(out_device).squeeze().detach().to(at::kFloat);
	//out_tensor1.to(out_device).squeeze().to(at::kFloat);
	//out_tensor = out_tensor.sub_(output.min()).div_(output.max()).mul(255).to(torch::kU8);
	
	
	return out_tensor2;
}


void Generator::saveRawData(const char* path, const torch::Tensor src)
{
	FILE* fp = NULL; //定义指针
	fp = fopen(path, "wb");
	auto accessor = src.accessor<float, 2>();
	//float* temp_arr = src.data<float>();
	/*int width = src.sizes()[1];
	int height = src.sizes()[0];

	float array[height][width];
	memcpy(array, src.data(), width * height * sizeof(float));*/

	for (int i = 0; i < accessor.sizes()[0]; i++)
	{
		for (int j = 0; j < accessor.sizes()[1]; j++)
		{
				//取出灰度图像中i行j列的点
			//float data = accessor[i][j];
			fwrite(&accessor[i][j], sizeof(float), 1, fp);
		}
	}

	fclose(fp);

}

void Generator::processFolder(std::vector<std::string> in_vector, std::string inFolder, std::string outFolder, int width, int height,float ratio)
{
	cv::Mat img_result;
	torch::Tensor temp_img;
	for (int i = 0; i < in_vector.size(); ++i)
	{

		//int height = 256;//原始图像的高
		//int width = 256;//原始图像的宽

		std::string path = inFolder + in_vector[i];

		img_result = get_raw_image(path.c_str(), width, height);

		temp_img = generate_raw(img_result,ratio);
		
		/*std::string out_path = outFolder + std::to_string(i) + ".png";
		cv::imwrite(out_path, temp_img);*/
		std::string out_path = outFolder + in_vector[i] ;
		saveRawData(out_path.c_str(), temp_img);

	}
	img_result.release();
}