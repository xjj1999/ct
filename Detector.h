#pragma 
#pragma once
#include <opencv2/opencv.hpp>
#undef slots
#include <torch/torch.h>
#define slots Q_SLOTS
#include <torch/script.h>

struct Object
{
    cv::Rect_<float> rect;
    int label;
    std::string prob;
};

class Detector
{
public:

    Detector(const std::string& model_file, const std::string& label_file, torch::DeviceType device_type);
    std::vector<std::string> load_class_names(const std::string& path);
    std::vector<Object> detect(const cv::Mat img);
    torch::Tensor PostProcessing(const torch::Tensor& detections, float conf_thres, float iou_thres);
    torch::Tensor GetBoundingBoxIoU(const torch::Tensor& box1, const torch::Tensor& box2);
    std::vector<float> imagePadding(const cv::Mat& src, cv::Mat& dst, const cv::Size& out_size);
    cv::Mat Detector::draw_objects(const cv::Mat& bgr, const std::vector<Object> objects);
    void Detector::processFolder(std::vector<std::string> in_vector, std::string inFolder, std::string outFolder);

private:
    std::vector<std::string> labels_;
    torch::jit::script::Module module;
    torch::DeviceType device_type;

};



